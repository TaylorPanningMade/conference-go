from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    URL = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    r = requests.get(URL, params=params, headers=headers)
    content = json.loads(r.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except {KeyError, IndexError}:
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    URL1 = "http://api.openweathermap.org/geo/1.0/direct"
    r_location = requests.get(URL1, params=params)
    loc_dict = json.loads(r_location.content)
    try:
        latitude = loc_dict[0]["lat"]
        longitude = loc_dict[0]["lon"]
    except (KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    URL2 = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(URL2, params=params)
    content = json.loads(response.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
